import glob
import logging
import os
import subprocess
import json

from collections import defaultdict

import click
import coloredlogs
from jinja2 import BaseLoader, Environment
import random
from pathlib import Path
import shutil

logger = logging.getLogger("some_logger")
coloredlogs.install(fmt="%(message)s")


def get_planets():
    # XXX Customize the location to get the planets from
    planet_files = glob.glob("public/content/planets/*.md")
    return [os.path.splitext(os.path.basename(f))[0] for f in planet_files]


def copy_folder(src, dst):
    # Check if destination folder exists, if not it creates one
    if not os.path.exists(dst):
        os.makedirs(dst)

    # Copy all files and subdirectories
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, False, None)
        else:
            shutil.copy2(s, d)


@click.command()
@click.option(
    "--planet",
    prompt=True,
    help="Choose your planet.",
    type=click.Choice(get_planets(), case_sensitive=False),
)
@click.option("--dir", help="The directory to use as input", default="public/content")
def character(planet, dir):
    dir = Path(dir)

    def get_random_file(folder, planet):
        files = glob.glob(os.path.join(dir, f"{folder}/*-{planet}.md"))
        return random.choice(files)

    for folder in ["personas", "instruments", "behaviours"]:
        with open(get_random_file(folder, planet)) as f:
            print(f.name)
            print(f.read())


@click.command()
@click.option("--content-dir", default="content", help="Directory containing content.")
@click.option("--output-dir", default="public/content", help="Output directory.")
@click.option(
    "--prompt", default=None, help="Prompt to run (if you want to run only one)"
)
@click.option("--model", default=None)
def generate(content_dir, output_dir, prompt, model):
    os.makedirs(output_dir, exist_ok=True)

    # Get the directory with prompts
    prompts_dir = os.path.join(content_dir, "prompts")

    # Get all the prompt files
    prompt_files = glob.glob(f"{prompts_dir}/*.md")

    if prompt:
        prompt_files = [p for p in prompt_files if p == prompt]

    # Process each prompt file
    for prompt_file in prompt_files:
        # Extract the verb, source, and filter from the filename
        verb, source, filter = os.path.basename(prompt_file).rstrip(".md").split("-")

        prompt_content = open(prompt_file).read()

        # Get all the source files and filter files
        source_files = glob.glob(f"{content_dir}/{source}s/*.md")
        filter_files = glob.glob(f"{content_dir}/{filter}s/*.md")

        # Iterate over each source and filter file
        for source_file in source_files:
            for filter_file in filter_files:
                # Parse markdown files
                source_content = open(source_file).read()
                filter_content = open(filter_file).read()

                # Construct the output filename preserving folder structure
                base_name = os.path.basename(source_file).split(".")[0]
                output_filename = f"{output_dir}/{source}s/{base_name}-{os.path.basename(filter_file)}"

                # Regenerate only if the output doesn't exist.
                if os.path.isfile(output_filename):
                    logger.debug(f"Skipping {output_filename}: file already exists")
                else:
                    logger.info(f"Generating {output_filename}")

                    # Call the function named 'verb' for source content and filter content
                    result = apply_verb(
                        verb, source_content, filter_content, prompt_content, model
                    )

                    if result:
                        logger.info(result)

                        # Ensure the directory exists
                        os.makedirs(f"{output_dir}/{source}s/", exist_ok=True)

                        # Store the result in the output directory
                        with open(output_filename, "w") as fp:
                            fp.write(result)


def apply_verb(verb, source_content, filter_content, prompt_content, model):
    if verb == "transform":
        return transform(source_content, filter_content, prompt_content, model)


def transform(source_content, filter_content, prompt_content, model):
    env = Environment(loader=BaseLoader)

    template = env.from_string(prompt_content)
    rendered_text = template.render(source=source_content, filter=filter_content)

    command = ["llm", rendered_text]
    if model:
        command.extend(["-m", model])
    result = subprocess.run(
        ["llm", rendered_text], stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    output = result.stdout.decode("utf-8") if result.stdout else None
    return output


@click.command()
@click.option("--input-dir", default="public/content")
@click.option("--output-dir", default="public")
@click.option("--planets-dir", default="content/planets")
@click.option("--copy-planets", default=True)
def manifest(input_dir, output_dir, planets_dir, copy_planets):
    input_dir = Path(input_dir)
    output_dir = Path(output_dir)
    planets_dir = Path(planets_dir)

    # XXX not sure it's the best location for this. Another command?
    if copy_planets:
        copy_folder(planets_dir, input_dir / "planets")
    

    def get_files(dir):
        file_dict = defaultdict(list)
        for root, dirs, files in os.walk(dir):
            for file in files:
                key = os.path.basename(root)
                file_dict[key].append(f"{root}/{file}".replace("public", ""))
        return file_dict
        
    manifest = get_files(input_dir)
    with open(output_dir / "manifest.json", "w") as f:
        json.dump(manifest, f, indent=4)


@click.group()
def cli():
    pass


cli.add_command(generate)
cli.add_command(character)
cli.add_command(manifest)


if __name__ == "__main__":
    cli()
