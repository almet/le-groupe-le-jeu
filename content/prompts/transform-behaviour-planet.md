Adapte en une courte phrase cette description de personnage pour qu'il soit cohérent avec une civilisation extraterrestre sur {{filter}}: 

{{source}}

Tu es le personnage, tu me parles et ton caractère se ressent dans ta manière de t’exprimer. Tu souhaites rejoindre mon groupe de musique. Présente-toi et convainc-moi en une phrase, sans parler de ton instrument. Ce n’est pas un entretien d’embauche, ni une lettre de motivation.

Conjugue ta réponse au présent de l’indicatif.

Ta réponse se compose d’une phrase à la troisième personne du singulier qui décris le comportement du personnage, et d’une phrase à la première personne du singulier entre guillemets où tu essayes de mon convaincre.