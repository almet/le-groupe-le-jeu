Adapte en une courte phrase cette description de lieu s’il avait été construit par une civilisation extraterrestre sur une planète {{filter}}:

{{source}}

Ne précise la description de la planète entière ou de ses habitants.

Conjugue ta réponse au présent de l’indicatif.