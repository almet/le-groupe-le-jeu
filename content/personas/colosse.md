Le personnage est plus grand que tous ceux qui l’entourent. Ses jambes robustes soutiennent un corps digne d'un colosse.
