module Main exposing (..)

import Browser
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (attribute, class, value)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode as Decode exposing (Decoder)
import Random
import Random.List exposing (choose)
import String


type alias Model =
    { places : List String
    , behaviours : List String
    , instruments : List String
    , personas : List String
    , planets : Dict String String
    , currentBehaviour : Maybe String
    , currentPlace : Maybe String
    , currentInstrument : Maybe String
    , currentPersona : Maybe String
    , currentPlanet : Maybe String
    , error : Maybe Http.Error
    }


init : flags -> ( Model, Cmd Msg )
init seed =
    let
        initialModel =
            { places = []
            , behaviours = []
            , instruments = []
            , personas = []
            , planets = Dict.fromList []
            , currentBehaviour = Nothing
            , currentPlace = Nothing
            , currentInstrument = Nothing
            , currentPersona = Nothing
            , currentPlanet = Nothing
            , error = Nothing
            }
    in
    ( initialModel
    , Http.get
        { url = "/manifest.json"
        , expect = Http.expectJson GotManifest manifestDecoder
        }
    )


type Msg
    = GotManifest (Result Http.Error (Dict String (List String)))
    | SetPlanet String
    | GenerateRandomCharacter
    | GotBehaviour (Result Http.Error String)
    | GotPlace (Result Http.Error String)
    | GotInstrument (Result Http.Error String)
    | GotPersona (Result Http.Error String)
    | PlaceSelected ( Maybe String, List String )
    | BehaviourSelected ( Maybe String, List String )
    | InstrumentSelected ( Maybe String, List String )
    | PersonaSelected ( Maybe String, List String )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotManifest (Err httpError) ->
            ( { model | error = Just httpError }, Cmd.none )

        GotManifest (Ok data) ->
            let
                newModel =
                    { model
                        | places = data |> Dict.get "places" |> Maybe.withDefault []
                        , behaviours = data |> Dict.get "behaviours" |> Maybe.withDefault []
                        , instruments = data |> Dict.get "instruments" |> Maybe.withDefault []
                        , personas = data |> Dict.get "personas" |> Maybe.withDefault []
                        , planets =
                            let
                                planetList =
                                    data |> Dict.get "planets" |> Maybe.withDefault []
                            in
                            planetList |> filesToDict
                    }
            in
            update (SetPlanet (newModel.planets |> Dict.keys |> List.head |> Maybe.withDefault "")) newModel

        GenerateRandomCharacter ->
            ( model
            , let
                currentPlanet =
                    case model.currentPlanet of
                        Just planet ->
                            planet

                        Nothing ->
                            ""

                withCurrentPlanet list =
                    List.filter (String.contains currentPlanet) list
              in
              Cmd.batch
                [ Random.generate PlaceSelected (model.places |> withCurrentPlanet |> choose)
                , Random.generate BehaviourSelected (model.behaviours |> withCurrentPlanet |> choose)
                , Random.generate InstrumentSelected (model.instruments |> withCurrentPlanet |> choose)
                , Random.generate PersonaSelected (model.personas |> withCurrentPlanet |> choose)
                ]
            )

        PlaceSelected ( returned, _ ) ->
            ( model
            , case returned of
                Just url ->
                    Http.get { url = url, expect = Http.expectString GotPlace }

                _ ->
                    Cmd.none
            )

        BehaviourSelected ( returned, _ ) ->
            ( model
            , case returned of
                Just url ->
                    Http.get { url = url, expect = Http.expectString GotBehaviour }

                _ ->
                    Cmd.none
            )

        InstrumentSelected ( returned, _ ) ->
            ( model
            , case returned of
                Just url ->
                    Http.get { url = url, expect = Http.expectString GotInstrument }

                _ ->
                    Cmd.none
            )

        PersonaSelected ( returned, _ ) ->
            ( model
            , case returned of
                Just url ->
                    Http.get { url = url, expect = Http.expectString GotPersona }

                _ ->
                    Cmd.none
            )

        GotBehaviour result ->
            case result of
                Ok content ->
                    ( { model | currentBehaviour = Just content }, Cmd.none )

                Err httpError ->
                    ( { model | error = Just httpError }, Cmd.none )

        GotPlace result ->
            case result of
                Ok content ->
                    ( { model | currentPlace = Just content }, Cmd.none )

                Err httpError ->
                    ( { model | error = Just httpError }, Cmd.none )

        GotInstrument result ->
            case result of
                Ok content ->
                    ( { model | currentInstrument = Just content }, Cmd.none )

                Err httpError ->
                    ( { model | error = Just httpError }, Cmd.none )

        GotPersona result ->
            case result of
                Ok content ->
                    ( { model | currentPersona = Just content }, Cmd.none )

                Err httpError ->
                    ( { model | error = Just httpError }, Cmd.none )

        SetPlanet name ->
            update GenerateRandomCharacter { model | currentPlanet = Just name }


manifestDecoder : Decoder (Dict String (List String))
manifestDecoder =
    Decode.dict (Decode.list Decode.string)


filesToDict : List String -> Dict String String
filesToDict paths =
    List.map
        (\path ->
            let
                splitted =
                    String.split "/" path

                fileNameWithExtension =
                    List.head (List.reverse splitted)

                fileName =
                    case fileNameWithExtension of
                        Just val ->
                            List.head (String.split "." val)

                        Nothing ->
                            Just ""
            in
            case fileName of
                Just key ->
                    ( key, path )

                Nothing ->
                    ( "", "" )
        )
        paths
        |> Dict.fromList



-- Views


viewFiles : Model -> Html msg
viewFiles model =
    ul []
        [ li [] [ text <| "Comportements: " ++ (model.behaviours |> List.length |> String.fromInt) ]
        , li [] [ text <| "Endroits: " ++ (model.places |> List.length |> String.fromInt) ]
        , li [] [ text <| "Instruments: " ++ (model.instruments |> List.length |> String.fromInt) ]
        , li [] [ text <| "Personas: " ++ (model.personas |> List.length |> String.fromInt) ]
        , li [] [ text <| "Planètes: " ++ (model.planets |> Dict.keys |> List.length |> String.fromInt) ]
        ]


viewCharacter : Model -> Html Msg
viewCharacter model =
    let
        renderElement name category =
            [ dt [] [ text name ]
            , dd [] [ category |> Maybe.withDefault "" |> text ]
            ]
    in
    div [ class "character" ]
        [ dl [] (renderElement "Persona" model.currentPersona)
        , dl [] (renderElement "Instrument" model.currentInstrument)
        , dl [] (renderElement "Comportement" model.currentBehaviour)
        , dl [] (renderElement "Lieu" model.currentPlace)
        ]


viewPlanetSelector : Model -> Html Msg
viewPlanetSelector model =
    let
        optionView planet =
            option
                [ value planet ]
                [ planet |> text ]
    in
    div []
        [ select
            [ onInput SetPlanet
            , class "form-select"
            ]
            (model.planets |> Dict.keys |> List.map optionView)
        ]


view : Model -> Html Msg
view model =
    div []
        [ header []
            [ nav []
                [ ul []
                    [ li [] [ strong [] [ "Le groupe, le jeu" |> text ] ]
                    ]
                , ul []
                    [ li [] [ button [ onClick GenerateRandomCharacter ] [ "Nouveau tirage" |> text ] ]
                    ]
                ]
            ]
        , node "main"
            [ class "container" ]
            [ viewPlanetSelector model
            , viewCharacter model
            ]
        , footer []
            [ viewFiles model
            ]
        ]


main : Program () Model Msg
main =
    Browser.element { init = init, update = update, view = view, subscriptions = always Sub.none }
