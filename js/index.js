import { Elm } from "/src/Main.elm"

import "./main.sass";

const app = Elm.Main.init({
  node: document.getElementById("main")
});
