Dans cette taverne cosmique, les Kuyay, extraterrestres semblables à d'immenses paresseux, sirotent paisiblement leur Pansyglow, une boisson locale à l'éclat pastel, et bavardent en langage lent.
