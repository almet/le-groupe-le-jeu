L'alien Kuyay présente une peau éclatante aux tons pastels ainsi qu'une fine fourrure douce et soyeuse, typiques chez les plus jeunes membres de sa race sur sa luxuriante planète natale.
