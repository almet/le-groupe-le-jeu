L'Alien Ædif a les épaules larges et musclées et ses bras épais semblent sculptés dans les structures durcies de sa colonie, témoignant de sa résilience face aux conditions extrêmes de sa planète.
