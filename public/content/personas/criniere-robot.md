L'Alien, autrefois doté d'une carapace noire, arbore désormais une exosquelette argentée brillante qui encadre une structure faciale marquée par les empreintes du temps.
