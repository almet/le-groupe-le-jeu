L'Alien, bien que ralenti par ses années, se déplace avec une aisance qui témoigne d'une vie de labeur; ses nageoires endurcies et colorées racontent une histoire de travail acharné.
