L'Alien, plus vaste que tous les Hoa qui l’entourent, utilise ses ailes puissantes pour soutenir un corps digne d'un colosse, joliment recouvert de couleurs vives.
