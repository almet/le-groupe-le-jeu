L'Alien Tøøø, autrefois d'un noir profond, s'est transformé en un nuage argenté qui se façonne et fluctue autour d'un noyau invisible, marqué par les ondulations changeantes du temps.
