L'Alien, un Ædif âgé, se déplace lentement, sa carapace usée et fissurée témoignant d'une existence passée sous le dur labeur des constructions souterraines et de surface.
