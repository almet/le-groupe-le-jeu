L'Alien, membre des Hoa, a une peau lisse, impeccable et colorée, et une bouche fine qui ressemble plus à une fente, typique de sa race de raies mantas amicales.
