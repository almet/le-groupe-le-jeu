L'Alien Kuyay, autrefois garni d'une épaisse fourrure sombre, est depuis recouvert d'une dense toison argentée qui entoure un visage creusé par les marques du temps.
