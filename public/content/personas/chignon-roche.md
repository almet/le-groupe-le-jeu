L'Alien Tøøø porte une teinte constante de rouge foncé, couleur qui chez sa race est signe de sévérité, et ses ondes télépathiques sont tranchantes et pénétrantes, ne laissant aucun doute sur la rigueur de sa pensée.

