L'Alien arbore un corps carré, orné de nageoires proéminentes et de textures rugueuses qui suggèrent une vie de défis physiques dans les profondeurs marines.
