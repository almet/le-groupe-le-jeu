L'Alien Tøøø, d'une teinte rare de gaz bleu pâle, scintille vivement lors de chaque communication télépathique, un phénomène couramment observable chez les membres plus jeunes de son espèce.
