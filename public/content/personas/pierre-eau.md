L'Alien possède d'amples nageoires dorsales et musclées, tandis que ses imposantes appendices latéraux semblent avoir été sculptés dans les coraux rouges de son habitat.
