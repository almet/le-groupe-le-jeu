L'Alien, malgré des mouvements ralentis par sa longue existence, a une allure qui, par ses appendices usés et remplis de micro-rayures, raconte une vie d'efforts incessants.
