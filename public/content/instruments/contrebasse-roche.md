Son instrument, gigantesque et creux, est conçu à partir d'une matière rocheuse, tendu de quatre cordes robustes frémissantes de notes graves, qui changent de couleurs au gré des émotions des Tøøø.
