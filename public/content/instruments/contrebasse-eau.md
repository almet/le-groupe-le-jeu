Son instrument, une œuvre magistrale taillée dans une structure corallienne rouge, possède quatre cordes robustes qui, lorsque tendues, libèrent des notes graves et envoûtantes.
