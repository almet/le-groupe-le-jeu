Son instrument est une percussion formée de deux coques assemblées, ayant sur sa face supérieure une série de zones disposées en cercle. Un trou sur sa face inférieure permet la résonance.
