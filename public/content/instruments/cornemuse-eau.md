Son instrument est un dispositif à vent extraterrestre, doté d'une poche et de plusieurs conduits, un d'eux générant une mélodie pendant que les autres émettent un bourdonnement harmonique constant.
