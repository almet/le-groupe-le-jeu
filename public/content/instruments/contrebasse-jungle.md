Son instrument, gigantesque et creux, est façonné dans un bois pastel aux nuances douces, et tend de quatre cordes épaisses qui, une fois pincées, produisent des notes profondément graves.
