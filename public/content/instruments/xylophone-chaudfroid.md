Son instrument est un assemblage complexe de lames en bois, disposées horizontalement et de tailles diverses, que les Ædif frappent d'habiles maillets pour créer leur mystique mélodie.
