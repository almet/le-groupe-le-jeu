Son instrument est une ambiguïté sonore agencée par une poche et divers conduits, l'un générant la mélodie tandis que les autres vibrent en un bourdon harmonique constant.
