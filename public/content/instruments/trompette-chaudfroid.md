Son instrument, modelé dans un métal méticuleusement poli aux courbes évasées, déchaîne des notes intenses et limpides, contrôlées par trois pistons et le souffle maîtrisé de l'Ædif musicien.
