Son instrument, imposant et arrimé au sol de la ville, abrite un labyrinthe complexe de cordes en acier que des marteaux frappent lorsqu'ils actionnent les nombreuses touches d'ivoire synthétique.
