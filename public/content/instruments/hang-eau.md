Son instrument est un tambour, constitué de deux coques attachées, arborant sur sa face supérieure une série de zones disposées en cercle. Un trou sur sa face inférieure lui permet de résonner.
