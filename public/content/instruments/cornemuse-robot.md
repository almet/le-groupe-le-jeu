Son instrument à vent, caractéristique de la civilisation Boulon, se compose d'une poche et de plusieurs tuyaux, l'un créant la mélodie pendant que les autres génèrent un bourdon harmonique constant.
