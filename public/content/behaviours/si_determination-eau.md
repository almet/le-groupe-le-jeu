Se mouvant lentement mais surement à travers l'océan, chaque battement de ses nageoires démontre sa détermination implacable. "Ecoute, je suis un Hoa lumineux et amical, et je te promets que ma présence éclatante et ma personnalité brillante donneront à ton groupe une énergie vibrante qu'il n'a jamais connue."

