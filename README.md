# Le goupe le jeu

## A faire

- [ ] Une interface Web mini qui affiche :
  - Vous arrivez sur la planète X, devant vous : 1, 2 lieux, ou rien.
  - Dans le lieu, description puis deux boutons :
    1. "discuter"
    2. "partir"
  - Lors du clic sur "discuter", ouvrir une fenêtre.

## Installation

Il y a plusieurs parties au projet : une partie qui génère les fichiers textes, et une partie qui permet d'afficher le jeu en tant que tel.

### Génération des fichiers textes

L'approche qui est prise ici est de générer de nouvelles données à partir de textes que nous avons écrits nous mêmes; en utilisant de l'IA générative.

Je m'explique : nous avons par exemple d'un côté une liste de planètes, avec leurs caractéristiques propres, et de l'autre une liste d'instruments de musique, avec leur description.

L'idée est de demander à un modèle de language de filtrer nos descriptions d'instruments au vu de chaque planète.

Le plus simple est d'installer `pipx` (avec la commande `brew install pipx && pipx ensurepath` sous mac par exemple). Puis d'installer les dépendances suivantes :

```bash
pipx install llm
pipx install pipenv
```

Puis il est possible d'installer les dépendances :

```bash
pipenv sync
```

#### Choisir et configurer son modèle de données

Le script utilise `llm` pour pouvoir communiquer avec des Large Language Models. Il est possible d'utiliser des modèles locaux ou de passer par des API externes.

##### Utilisation de l'API d'OpenAI

Le plus simple est d'utiliser l'API de OpenAI (mais c'est payant).

Donnez les clés d'API à `llm`, vous pouvez les générer [depuis ici](https://platform.openai.com/account/api-keys).

```bash
llm keys set openai
```

Utilisons GTP4 par défaut : 
```bash
llm models default gpt-4
```

##### Utilisation d'un autre modèle

Il est aussi possible d'utiliser d'autres modèles, et entre autres un modèle qui tourne en local sur votre machine. Par exemple le modèle Vigogne, qui parle Français.

Voici comment l'installer :
```bash
brew install wget
wget https://huggingface.co/TheBloke/Vigogne-2-7B-Chat-GGUF/resolve/main/vigogne-2-7b-chat.Q4_K_M.gguf
llm llama-cpp add-model vigogne-2-7b-chat.Q4_K_M.gguf -a vigogne
llm models default vigogne
```

### Lancer l'application web

Pour l'application web, vous pouvez lancer cette commande, et cliquer sur le lien qui apparaitra.

```bash
yarn run vite
```

## Lancer les scripts

### Générer les fichiers

```bash
pipenv run python cli.py generate
```

Le script regénère les fichiers qui sont manquants dans le dossier `output`. Pour regénérer uniquement les fichiers qui viennent d'un prompt, vous pouvez par exemple lancer :

```bash
pipenv run python cli.py generate --prompt content/prompts/transform-place-planet.md
```

### Avoir un aperçu des caractères

Il est possible d'avoir un aperçu d'un caractère aléatoire en faisant :

```bash
pipenv run python cli.py character
```

On peut passer l'argument `--planet` pour lui spécifier une planète.